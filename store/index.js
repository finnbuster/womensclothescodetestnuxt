export const state = () => ({
  endpoint: 'https://api.jsonbin.io/b/5cae9a54fb42337645ebcad3',
  products: [],
  selectedFilter: ''
})

export const actions = {
  // Pull in all products on server init and commit them to the store
  async nuxtServerInit({ commit, state }) {
    const products = await this.$axios.get(state.endpoint)
    commit('setProducts', products.data)
  }
}

export const mutations = {
  setProducts(state, data) {
    state.products = data
  },
  setFilter(state, size) {
    state.selectedFilter = size
  }
}

export const getters = {
  // To get all the sizes of products, we need to filter through all products and map out all the sizes and then use Set to pull out only unique values
  sizeFilters(state) {
    const sizes = []
    state.products.map(function(product) {
      product.size.map(function(size) {
        sizes.push(size)
      })
    })
    return [...new Set(sizes)]
  },
  filteredProducts(state) {
    // Return all products if no filter selected (a blank string) otherwise filtler products based on the selected filter string
    return state.selectedFilter
      ? state.products.filter(product =>
          product.size.find(size => size === state.selectedFilter)
        )
      : state.products
  }
}
